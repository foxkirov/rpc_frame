import pika
import uuid
import os
import time
from random import randint
rabbit_mq_user = os.getenv('RABBITMQ_DEFAULT_USER', 'user')
rabbit_mq_pass = os.getenv('RABBITMQ_DEFAULT_PASS', 'pass')

credentials = pika.PlainCredentials(rabbit_mq_user, rabbit_mq_pass, )


def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)


class FibonacciRpcClient(object):
    def __init__(self):
        while True:
            try:
                self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbit', credentials=credentials))
            except pika.exceptions.AMQPConnectionError:
                print('waiting rabbitMQ... ')
                time.sleep(1)
            else:
                break

        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(on_message_callback=self.on_response, auto_ack=False,
                                   queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            ch.basic_ack(delivery_tag=method.delivery_tag)
            self.response = body
        else:
            ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)

    def call(self, n):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key='rpc_queue',
                                   properties=pika.BasicProperties(
                                         reply_to=self.callback_queue,
                                         correlation_id=self.corr_id,
                                         ),
                                   body=str(n))
        while self.response is None:
            self.connection.process_data_events()
        return int(self.response)


def main():
    fibonacci_rpc = FibonacciRpcClient()
    while True:
        val = randint(0, 30)
        print(f" [x] Requesting fib({val})")
        response = fibonacci_rpc.call(val)
        if response == fib(val):
            print(f" [.] Got: fibonacci({val})={response}")
        else:
            print(' [x] Something wrong...')
        time.sleep(0.1)


if __name__ == '__main__':
    main()
