import pika
import os
import sys
import time
from random import randint

rabbit_mq_user = os.getenv('RABBITMQ_DEFAULT_USER', 'user')
rabbit_mq_pass = os.getenv('RABBITMQ_DEFAULT_PASS', 'pass')
credentials = pika.PlainCredentials(rabbit_mq_user, rabbit_mq_pass, )

time.sleep(5)

while True:
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbit', credentials=credentials))
    except pika.exceptions.AMQPConnectionError:
        print('waiting rabbitMQ... ')
        time.sleep(1)
    else:
        break


channel = connection.channel()

channel.queue_declare(queue='rpc_queue')


def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)


def on_request(ch, method, props, body):
    n = int(body)
    choice = randint(0, 20)
    if choice == 0:
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=True)
        print(" [.] fib(%s) - expectable error emulating" % (n,))
    elif choice == 1:
        print(" [.] fib(%s) - unexpectable error emulating" % (n,))
        sys.exit(1)
    else:
        print(" [.] fib(%s)" % (n,))
        response = fib(n)

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         properties=pika.BasicProperties(correlation_id=props.correlation_id),
                         body=str(response))

        ch.basic_ack(delivery_tag=method.delivery_tag)


def main():
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(on_message_callback=on_request, queue='rpc_queue')

    print(" [x] Awaiting RPC requests")
    channel.start_consuming()


if __name__ == '__main__':
    main()
